import { mapGetters } from "vuex";
import PageTemplate from "../components/UI/PageTemplate";

export default {
  components: {
    PageTemplate
  },
  data() {
    return {
      o: "aazz"
    };
  },
  computed: {
    ...mapGetters("auth", {
      user_data: "user_data"
    })
  }
};
