import Vue from "nativescript-vue";
import {isIOS} from "tns-core-modules/platform";
import App from "./components/App";
import axios from "./config/axios";
import BottomNavigationBar from "nativescript-bottom-navigation/vue";

import VueDevtools from "nativescript-vue-devtools";
import store from "./store/store";
import global_mixin from "./config/global_mixin";
import {sysAudio} from "./commons";

import "./assets/main.css";

// Moment.js
const moment = require('moment');
require('moment/locale/ru');
Vue.use(require('vue-moment'), {
  moment
});

// Сканер кодов
Vue.registerElement(
  "BarcodeScanner",
  () => require("nativescript-barcodescanner").BarcodeScannerView
);

Vue.registerElement("Ripple", () => require("nativescript-ripple").Ripple);

// Декоратор клавиатуры для iOS
if (isIOS) {
  Vue.registerElement(
    "PreviousNextView",
    () => require("nativescript-iqkeyboardmanager").PreviousNextView
  );
  Vue.registerElement(
    "TextViewWithHint",
    () => require("nativescript-iqkeyboardmanager").TextViewWithHint
  );
}

// Card
Vue.registerElement(
  "CardView",
  () => require("@nstudio/nativescript-cardview").CardView
);
import "nativescript-tailwind/dist/tailwind.css";
Vue.use(BottomNavigationBar);

Vue.use(VueDevtools);

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === "production";

Vue.mixin(global_mixin);
Vue.prototype.$http = axios;
Vue.prototype.$sysAudio = sysAudio;

import { TNSFontIcon, fonticon } from "nativescript-fonticon";

TNSFontIcon.debug = true;
TNSFontIcon.paths = {
  fa: "./assets/css/font-awesome.css"
};
TNSFontIcon.loadCss();

Vue.filter("fonticon", fonticon);

Vue.registerElement(
  "RadSideDrawer",
  () => require("nativescript-ui-sidedrawer").RadSideDrawer
);

new Vue({
  store,
  render: h => h("frame", [h(App)])
}).$start();
